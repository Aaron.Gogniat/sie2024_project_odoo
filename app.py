#everything in comment is things I've tried throughout this project
import xmlrpc.client #Odoo
import requests
from flask import Flask, render_template, request #, send_file
#import os
#import pandas
#import xlrd
#from openpyxl import load_workbook
from twilio.rest import Client #Twilio
#from werkzeug.utils import secure_filename
import time

######################### Get employees' info from Odoo ######################################################################################################################################
# Retrieve employee information for all employees from Odoo
def get_all_employee_from_odoo():  
    # Search for all employees
    employee_ids = models.execute_kw(
        db, uid, password, 'hr.employee', 'search', 
        [[]]
    )
    # Retrieve information for each employee
    employee_info = models.execute_kw(
        db, uid, password, 'hr.employee', 'read', 
        [employee_ids], {'fields': ['id', 'work_phone', 'name']}
    ) 
    # Create a list of dictionaries containing employee information
    employee_info_odoo = []
    
    for employee in employee_info:
        # Split the full name into first name and last name
        full_name = employee['name']
        # Split at the first space
        split_name = full_name.split(' ', 1) 
        # Assign first name and last name to separate fields
        firstName = split_name[0] if split_name else None
        lastName = split_name[1] if len(split_name) > 1 else None
        # Add employee to the list
        employee_info_odoo.append({
            'id': str(employee['id']),
            'firstName': firstName,
            'lastName': lastName,
            'displayName': employee['name'],
            'workPhone': employee['work_phone'],
        })
        
    return employee_info_odoo

######################### Get employees' info from Deputy ######################################################################################################################################
def get_all_employee_from_deputy():
    url = f"{urlbase_deputy}/api/management/v2/employees?fieldMask=id,firstName,lastName,displayName,contact"

    response = requests.get(url, headers=headers)

    # Check if the request was successful
    if response.status_code == 200:
        # Parse JSON response
        response_data = response.json()

        # Extract the list of employees from the response
        employees = response_data.get('result', [])

        # Create an empty list to store employee info
        employee_info_deputy = []

        # Iterate over employees and extract required info
        for employee in employees:
            data = employee.get('data', {})
            id_1 = data.get('id')
            first_name = data.get('firstName')
            last_name = data.get('lastName')
            display_name = data.get('displayName')
            contact_data = data.get('contact', {})
            notes = contact_data.get('notes')
        
            employee_info_deputy.append({
                'id_1' : id_1,
                'id' : str(notes),
                'firstName': first_name,
                'lastName': last_name,
                'displayName' : display_name
            })
        #print("RESPONSE FROM DEPUTY :", response.text)
        return employee_info_deputy 
    else:
        # Unsuccessful request
        print("Failed to fetch employee names from Deputy API:", response.text)
        return None

############################### Check if there is differences to send the modified, new ones or the ones to delete to deputy ####################################################################
def employee_list_to_add(odoo_employees, deputy_employees):
    # Create an empty list to store new employee info
    new_employees_list = []
    
    for employee_odoo in odoo_employees :
        # Reset counter to 0
        n = 0
        for employee_deputy in deputy_employees :
            # Checks to see if they have an id
            if employee_deputy['id'] != 'None' :  
                # If there is someone with the same id count them
                if employee_deputy['id'] == employee_odoo['id'] :
                    n += 1
        # If there isn't anyone with the same id add their information to the list to add them
        if n == 0 :
            new_employees_list.append({
                'id': employee_odoo['id'],
                'firstName': employee_odoo['firstName'],
                'lastName': employee_odoo['lastName'],
                'displayName': employee_odoo['displayName']
            })
            
    return new_employees_list

def employee_list_to_update(odoo_employees, deputy_employees):
    # Create a list to store modified employees
    modified_employees_list = []
    
    for odoo_employee in odoo_employees :
        for deputy_employee in deputy_employees :
            if deputy_employee['id'] != 'None' : 
                # Don't delete my deputy account as its not in Odoo anyway
                if deputy_employee['id'] == odoo_employee['id'] :
                    # Compare fields to check for modifications
                    if ((odoo_employee['firstName'] != deputy_employee['firstName']) or
                        (odoo_employee['lastName'] != deputy_employee['lastName']) or
                        (odoo_employee['displayName'] != deputy_employee['displayName'])):
                        # Add modified employee to the list
                        modified_employees_list.append({
                            'id': deputy_employee['id_1'],
                            'firstName': odoo_employee['firstName'],
                            'lastName': odoo_employee['lastName'],
                            'displayName': odoo_employee['displayName'],
                        })

    return modified_employees_list

def employee_list_to_delete(odoo_employees, deputy_employees) :
    # Create a list to store deleted employees
    delete_employees_list = []
    
    for employee_deputy in deputy_employees :
        # Set the counter to 0
        n = 0
        
        # Don't delete the Website Administrator (me)
        if employee_deputy['id_1'] != '1' :
            for employee_odoo in odoo_employees :
                # Check if there is anyone with the same id
                if employee_deputy['id'] == employee_odoo['id'] :  
                    n += 1
            # If nobody has the same id in Odoo, add them to the delete list
            if n == 0 :
                delete_employees_list.append({
                    'id': employee_deputy['id_1'],
                    'firstName': employee_deputy['firstName'],
                    'lastName': employee_deputy['lastName'],
                    'displayName': employee_deputy['displayName']
                })
            
    return delete_employees_list
     

#################################### Add or modifiy employees to Deputy #########################################################################################################################
def send_new_employees_to_deputy(employees_to_add):
    url = f"{urlbase_deputy}/api/management/v2/employees"
    
    # Add new employees
    for employee in employees_to_add:
        payload = {
            "firstName": employee["firstName"],
            "lastName": employee["lastName"],
            "contact" : {
                "notes" : employee["id"]
            },
            "primaryLocation": { 
                "id": "1"  # Needed to be able to send 
            }
        }
        headers_send = headers
        headers_send["content-type"] = "application/json"

        response = requests.post(url, json=payload, headers=headers_send)

        print(response.text)
        
        #print ('JSON DATA : ', response)
        if response.status_code == 200:
            print(f"New employee {employee['firstName']} {employee['lastName']} added successfully to Deputy")
        else:
            print(f"Failed to add new employee {employee['firstName']} {employee['lastName']} to Deputy")

            
def update_modified_employees_on_deputy(employees_to_update):
    # Update modified employees
    for employee in employees_to_update:
        # Get the id to add to the link
        id_employee = int(employee["id"])
        #print(employee)
        #print(id_employee)
        
        url = f"{urlbase_deputy}/api/management/v2/employees/{id_employee}"
        #print(url)
        
        data = {
            "firstName": employee["firstName"],
            "lastName": employee["lastName"],
            "displayName": employee["displayName"],
            "primaryLocation": { 
                "id": "1"  # Needed to be able to send 
            }
        }
        response = requests.patch(url, headers=headers, json=data)
        
        #print ('JSON DATA : ', response)
        if response.status_code == 200:
            print(f"Employee {employee['firstName']} {employee['lastName']} updated successfully in Deputy")
        else:
            print(f"Failed to update employee {employee['firstName']} {employee['lastName']} in Deputy")
            
def delete_employees_on_deputy(employees_to_delete) :
    # Update modified employees
    for employee in employees_to_delete:
        # Get the id to add to the link
        id_employee = int(employee["id"])
        #print(employee)
        #print(id_employee)
        
        url = f"{urlbase_deputy}/api/v1/supervise/employee/{id_employee}/terminate" #"{urlbase_deputy}/api/v1/supervise/employee/{id_employee}/delete"
        #=> does not work ! can only terminate them which archive them which will still delete them from the 'employee' deputy database

        response = requests.post(url, headers=headers)

        #print ('JSON DATA : ', response.text)
        if response.status_code == 200:
            print(f"Employee {employee['firstName']} {employee['lastName']} archived successfully in Deputy")
        else:
            print(f"Failed to archived employee {employee['firstName']} {employee['lastName']} in Deputy")
            
##################################### Send link to the odoo employees with Twilio ############################################################################################################
def send_the_link_to_employee_odoo (odoo_employees,link) :
        client = Client(account_sid, auth_token)
        # Send a message to each employee
        for employee in odoo_employees:
            # Retrieve the work phone number
            work_phone = employee['workPhone']  
            message = client.messages.create(
                from_='whatsapp:+14155238886',
                body=f'Voici le planning mis à jour. Bonne journée {link}',
                to=f'whatsapp:{work_phone}'
            )
            time.sleep(1.1) #sleep 1 second because Twilio has a 1rps rate, 1.1 second just to be sure
            print("Message sent to employee", employee['displayName'], "at", work_phone)
  

############################################### MAIN ############################################################################################################################################
# No main function because of flask
######################### Authenticate with Odoo API ###########################################################################################

# Connection Configuration : to change with your own information 
# in order to link your Odoo database
url_odoo = 'http://localhost:8069/'# To change if it is not on localhost
db = '' # Enter your database name
username = '' # Enter your username/adresse email
password = '' # Enter your password

# Connect to the Odoo API
common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url_odoo))

# Authenticate with the Odoo API
uid = common.authenticate(db, username, password, {})

if uid:
    # Connected and authenticated successfully
    models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url_odoo))
    print('Authenticated with Odoo API')
else:
    print('Authentication failed')
    exit(1)
    
########################## Authenticate with Deputy API ######################################################################################
# Connection Configuration
urlbase_deputy = ""  # Enter your URL with the identification, it should look like this  https://{install}.{geo}.deputy.com
url_deputy = f"{urlbase_deputy}/api/v1/me"
# print(url_deputy)
 
headers = {
    "accept": "application/json",
    "authorization": "Bearer " # Enter the Access Token after "Bearer", for example "Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx"
}
response = requests.get(url_deputy, headers=headers)

if response.status_code == 200:
    print('Connected to Deputy API successfully')
else:
    print('Failed to connect to Deputy API')
    exit(1)
    
############################ Authenticate with Twilio API ##########################################################################################
# Twilio Authentication
account_sid = '' # Enter your Account SID
auth_token = '' # Enter your Auth Token

########################### THE EXECUTION LINES ################################################################################################
# Retrieve employees from Odoo and Deputy
odoo_employees = get_all_employee_from_odoo()
print ("ODOO EMPLOYEES :", odoo_employees)
deputy_employees = get_all_employee_from_deputy()
print ("DEPUTY EMPLOYEES :", deputy_employees)

# Compare employee lists to add
new_employees= employee_list_to_add(odoo_employees, deputy_employees)
print ("NEW EMPLOYEES :", new_employees)
# Check if the list is empty
if new_employees :
    # Update Deputy with new employees
    send_new_employees_to_deputy(new_employees)

# Compare employee lists to update
modified_employees = employee_list_to_update(odoo_employees, deputy_employees)
print ("MODIFIED EMPLOYEES :", modified_employees)
# Check if the list is empty
if modified_employees :
    # Update Deputy with modified employees
    update_modified_employees_on_deputy(modified_employees)
    
# Compare employee lists to delete
delete_employees = employee_list_to_delete(odoo_employees, deputy_employees)
print ("DELETED EMPLOYEES :", delete_employees)
# Check if the list is empty
if delete_employees :
    # Update Deputy with deleted employees
    delete_employees_on_deputy(delete_employees)

    
    ############################### FLASK #############################################################################################################
app = Flask(__name__)

@app.route('/')
# Wait for the link to be sent
def index():
    return render_template('homepage.html')

@app.route('/send-link', methods=['POST'])
def send_link():
    # Retrieve the link
    link = request.form['link']
    # Call the function to send the link to Odoo employees
    send_the_link_to_employee_odoo(odoo_employees, link)
    return "Link sent successfully!"

if __name__ == '__main__':
    app.run(debug=True)